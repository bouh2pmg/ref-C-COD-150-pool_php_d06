<?php

interface IUnit
{
    public function equip($weapon);
    public function attack($target);
    public function receiveDamage($dmg);
    public function moveCloseTo($target);
    public function recoverAP();
}