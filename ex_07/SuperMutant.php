<?php

include_once("AMonster.php");

class SuperMutant extends AMonster
{
    static private $id = 1;
    
    public function __construct()
    {        
        parent::__construct("SuperMutant #" . SuperMutant::$id++);
        $this->hp = 170;
        $this->ap = 20;
        $this->damage = 60;
        $this->apcost = 20;
        echo $this->name . ": Roaarrr !\n";
    }

    public function __destruct()
    {
        echo $this->name . ": Urgh !\n";
    }

    public function recoverAP()
    {
        if ($this->hp <= 0)
            return false;
        $this->ap += 7;
        if ($this->ap > 50)
            $this->ap = 50;
        $this->hp += 10;
        if ($this->hp > 170)
            $this->hp = 170;
    }

}